package jp.alhinc.hayashi_keita.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		//支店定義ファイル読み込み
		if (!(inputFile(args[0], branchNames, branchSales, "branch.lst", "支店", "[0-9]{3}"))) {
			return;
		}

		//支店定義ファイル読み込み
		if (!(inputFile(args[0], commodityNames, commoditySales, "commodity.lst", "商品", "^[A-Za-z0-9]{8}"))) {
			return;
		}

		//全件取得、List宣言
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//売り上げファイル判定
		for (int i = 0; i < files.length; i++) {

			//File型をString型に変換
			String fileName = files[i].getName();

			if (files[i].isFile() && fileName.matches("[0-9]{8}\\.rcd")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		for (int j = 0; j < rcdFiles.size() - 1; j++) {

			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(j + 1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売り上げファイル読み込み
		for (int i = 0; i < rcdFiles.size(); i++) {

			BufferedReader br = null;

			try {
				//支店コードと売上金額を保持するString型のList
				List<String> fileContents = new ArrayList<>();

				File fileInfo = rcdFiles.get(i);
				FileReader fr = new FileReader(fileInfo);
				br = new BufferedReader(fr);

				String line;

				while ((line = br.readLine()) != null) {
					fileContents.add(line);
				}
				//↑までで支店コード(0)、商品コード(1)、売上額(2)の抽出完了！

				if (fileContents.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				if (!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				if (!commodityNames.containsKey(fileContents.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//読み込んだ情報を型変換
				long fileSale = Long.parseLong(fileContents.get(2));

				if (!fileContents.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店ごとの合計金額合算処理
				Long saleAmount = branchSales.get(fileContents.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(fileContents.get(1)) + fileSale;

				if (saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//Mapに追加
				branchSales.put(fileContents.get(0), saleAmount);
				commoditySales.put(fileContents.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}
		//支店別集計ファイル出力メソッド呼び出し
		if (!(outputFile(args[0], branchNames, branchSales, "branch.out"))) {
			return;
		}

		//商品別集計ファイル出力メソッド呼び出し
		if (!(outputFile(args[0], commodityNames, commoditySales, "commodity.out"))) {
			return;
		}
	}

	//定義ファイル読み込みメソッド
	public static boolean inputFile(String path, Map<String, String> names, Map<String, Long> sales,
			String fileNames, String definition, String match) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileNames);
			if (!file.exists()) {
				System.out.println(definition + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if ((items.length != 2) || (!(items[0].matches(match)))) {
					System.out.println(definition + "定義ファイルのフォーマットが不正です");
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], (long) 0);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}

	//集計ファイル出力メソッド
	public static boolean outputFile(String path, Map<String, String> names, Map<String, Long> sales,
			String fileNames) {

		BufferedWriter bw = null;

		try {
			File file = new File(path, fileNames);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//keyを取り出して対応するそれぞれのMapから値を取り出す
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true; //returnで返す値はメソッドの型と同じ型
	}
}
